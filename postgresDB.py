
from sqlalchemy import create_engine
from sqlalchemy import Table, Column, String, Integer, ForeignKey, Boolean
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, relation
from sqlalchemy.orm import relationship


base = declarative_base()


class Agent(base):
    __tablename__ = 'agent'
    id = Column(Integer, primary_key=True)
    name = Column(String)
    language = Column(String)
    gender = Column(String)
    intents = relationship("Intent", back_populates="agent")
    entities = relationship("Entity", back_populates="agent")

class Intent(base):
    __tablename__ = 'intent'
    id = Column(Integer, primary_key=True)
    name = Column(String)
    agent_id = Column(Integer, ForeignKey('agent.id'))
    agent = relationship("Agent", back_populates="intents")
    priority_id = Column(Integer, ForeignKey('priority.id'))
    priority = relationship("Priority", back_populates="intents")
    questions = relationship("Question", back_populates="intent")
    responses = relationship("Response", back_populates="intent")
    action = relationship("Action", uselist=False, back_populates="intent")

class Priority(base):
    __tablename__ = 'priority'
    id = Column(Integer, primary_key=True)
    name = Column(String)
    priority = Column(Integer)
    intents = relationship("Intent", back_populates="priority")


class Question(base):
    __tablename__ = 'question'
    id = Column(Integer, primary_key=True)
    question = Column(String)
    intent_id = Column(Integer, ForeignKey('intent.id'))
    intent = relationship("Intent", back_populates="questions")

class Response(base):
    __tablename__ = 'response'
    id = Column(Integer, primary_key=True)
    response = Column(String)
    intent_id = Column(Integer, ForeignKey('intent.id'))
    intent = relationship("Intent", back_populates="responses")

class Action(base):
    __tablename__ = 'action'
    id = Column(Integer, primary_key=True)
    name = Column(String)
    parameters = relationship("Parameter", back_populates="action")
    intent_id = Column(Integer, ForeignKey('intent.id'))
    intent = relationship("Intent", back_populates="action")

class Parameter(base):
    __tablename__ = 'parameter'
    id = Column(Integer, primary_key=True)
    name = Column(String)
    isList = Column(Boolean)
    prompt = Column(String)
    action_id = Column(Integer, ForeignKey('action.id'))
    action = relationship("Action", back_populates="parameters")
    entity_id = Column(Integer, ForeignKey('entity.id'))
    entity = relationship("Entity", back_populates="parameters")

EntityRelationship = Table(
    'entityrelationship', base.metadata,
    Column('parentid', Integer, ForeignKey('entity.id')),
    Column('childid', Integer, ForeignKey('entity.id'))
    )


class Entity(base):
    __tablename__ = 'entity'
    id = Column(Integer, primary_key=True)
    name = Column(String)
    parameters = relationship("Parameter", back_populates="entity")
    agent_id = Column(Integer, ForeignKey('agent.id'))
    agent = relationship("Agent", back_populates="entities")
    synonymreferences = relationship("SynonymReference", back_populates="entity")
    childs = relation(
                    'Entity',secondary=EntityRelationship,
                    primaryjoin=EntityRelationship.c.childid==id,
                    secondaryjoin=EntityRelationship.c.parentid==id)

class SynonymReference(base):
    __tablename__ = 'synonymreference'
    id = Column(Integer, primary_key=True)
    name = Column(String)
    entity_id = Column(Integer, ForeignKey('entity.id'))
    entity = relationship("Entity", back_populates="synonymreferences")
    synonyms = relationship("Synonym", back_populates="synonymreference")

class Synonym(base):
    __tablename__ = 'synonym'
    id = Column(Integer, primary_key=True)
    name = Column(String)
    synonymreference_id = Column(Integer, ForeignKey('synonymreference.id'))
    synonymreference = relationship("SynonymReference", back_populates="synonyms")




def connect(user, password, db, host='localhost', port=5432):
    # We connect with the help of the PostgreSQL URL
    # postgresql://federer:grandestslam@localhost:5432/tennis
    url = 'postgresql://{}:{}@{}:{}/{}'
    url = url.format(user, password, host, port, db)

    return url

db_string = connect('postgres', 'postgres', 'KnowledgeBaseDB')

db = create_engine(db_string)
Session = sessionmaker(db)
session = Session()
base.metadata.create_all(db)
'''
# Create
doctor_strange = Film(title="Doctor Strange", director="Scott Derrickson", year="2016")
session.add(doctor_strange)
session.commit()

# Read
films = session.query(Film)
for film in films:
    print(film.title)

# Update
doctor_strange.title = "Some2016Film"
session.commit()


'''
'''
# Delete
session.delete(doctor_strange)  
session.commit()  
'''