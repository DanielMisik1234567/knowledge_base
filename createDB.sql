--
-- PostgreSQL database dump
--

-- Dumped from database version 10.1
-- Dumped by pg_dump version 10.1

-- Started on 2017-11-23 23:00:00

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 2905 (class 1262 OID 16393)
-- Name: KnowledgeBaseDB; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE "KnowledgeBaseDB" WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'English_United States.1252' LC_CTYPE = 'English_United States.1252';


ALTER DATABASE "KnowledgeBaseDB" OWNER TO postgres;

\connect "KnowledgeBaseDB"

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 12924)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2907 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 209 (class 1259 OID 16730)
-- Name: action; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE action (
    id integer NOT NULL,
    name character varying,
    intent_id integer
);


ALTER TABLE action OWNER TO postgres;

--
-- TOC entry 208 (class 1259 OID 16728)
-- Name: action_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE action_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE action_id_seq OWNER TO postgres;

--
-- TOC entry 2908 (class 0 OID 0)
-- Dependencies: 208
-- Name: action_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE action_id_seq OWNED BY action.id;


--
-- TOC entry 197 (class 1259 OID 16639)
-- Name: agent; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE agent (
    id integer NOT NULL,
    name character varying,
    language character varying,
    gender character varying
);


ALTER TABLE agent OWNER TO postgres;

--
-- TOC entry 196 (class 1259 OID 16637)
-- Name: agent_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE agent_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE agent_id_seq OWNER TO postgres;

--
-- TOC entry 2909 (class 0 OID 0)
-- Dependencies: 196
-- Name: agent_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE agent_id_seq OWNED BY agent.id;


--
-- TOC entry 203 (class 1259 OID 16682)
-- Name: entity; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE entity (
    id integer NOT NULL,
    name character varying,
    agent_id integer
);


ALTER TABLE entity OWNER TO postgres;

--
-- TOC entry 202 (class 1259 OID 16680)
-- Name: entity_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE entity_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE entity_id_seq OWNER TO postgres;

--
-- TOC entry 2910 (class 0 OID 0)
-- Dependencies: 202
-- Name: entity_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE entity_id_seq OWNED BY entity.id;


--
-- TOC entry 210 (class 1259 OID 16744)
-- Name: entityrelationship; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE entityrelationship (
    parentid integer,
    childid integer
);


ALTER TABLE entityrelationship OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 16661)
-- Name: intent; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE intent (
    id integer NOT NULL,
    name character varying,
    agent_id integer,
    priority_id integer
);


ALTER TABLE intent OWNER TO postgres;

--
-- TOC entry 200 (class 1259 OID 16659)
-- Name: intent_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE intent_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE intent_id_seq OWNER TO postgres;

--
-- TOC entry 2911 (class 0 OID 0)
-- Dependencies: 200
-- Name: intent_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE intent_id_seq OWNED BY intent.id;


--
-- TOC entry 214 (class 1259 OID 16775)
-- Name: parameter; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE parameter (
    id integer NOT NULL,
    name character varying,
    "isList" boolean,
    prompt character varying,
    action_id integer,
    entity_id integer
);


ALTER TABLE parameter OWNER TO postgres;

--
-- TOC entry 213 (class 1259 OID 16773)
-- Name: parameter_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE parameter_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE parameter_id_seq OWNER TO postgres;

--
-- TOC entry 2912 (class 0 OID 0)
-- Dependencies: 213
-- Name: parameter_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE parameter_id_seq OWNED BY parameter.id;


--
-- TOC entry 199 (class 1259 OID 16650)
-- Name: priority; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE priority (
    id integer NOT NULL,
    name character varying,
    priority integer
);


ALTER TABLE priority OWNER TO postgres;

--
-- TOC entry 198 (class 1259 OID 16648)
-- Name: priority_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE priority_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE priority_id_seq OWNER TO postgres;

--
-- TOC entry 2913 (class 0 OID 0)
-- Dependencies: 198
-- Name: priority_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE priority_id_seq OWNED BY priority.id;


--
-- TOC entry 205 (class 1259 OID 16698)
-- Name: question; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE question (
    id integer NOT NULL,
    question character varying,
    intent_id integer
);


ALTER TABLE question OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 16696)
-- Name: question_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE question_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE question_id_seq OWNER TO postgres;

--
-- TOC entry 2914 (class 0 OID 0)
-- Dependencies: 204
-- Name: question_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE question_id_seq OWNED BY question.id;


--
-- TOC entry 207 (class 1259 OID 16714)
-- Name: response; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE response (
    id integer NOT NULL,
    response character varying,
    intent_id integer
);


ALTER TABLE response OWNER TO postgres;

--
-- TOC entry 206 (class 1259 OID 16712)
-- Name: response_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE response_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE response_id_seq OWNER TO postgres;

--
-- TOC entry 2915 (class 0 OID 0)
-- Dependencies: 206
-- Name: response_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE response_id_seq OWNED BY response.id;


--
-- TOC entry 216 (class 1259 OID 16796)
-- Name: synonym; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE synonym (
    id integer NOT NULL,
    name character varying,
    synonymreference_id integer
);


ALTER TABLE synonym OWNER TO postgres;

--
-- TOC entry 215 (class 1259 OID 16794)
-- Name: synonym_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE synonym_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE synonym_id_seq OWNER TO postgres;

--
-- TOC entry 2916 (class 0 OID 0)
-- Dependencies: 215
-- Name: synonym_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE synonym_id_seq OWNED BY synonym.id;


--
-- TOC entry 212 (class 1259 OID 16759)
-- Name: synonymreference; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE synonymreference (
    id integer NOT NULL,
    name character varying,
    entity_id integer
);


ALTER TABLE synonymreference OWNER TO postgres;

--
-- TOC entry 211 (class 1259 OID 16757)
-- Name: synonymreference_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE synonymreference_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE synonymreference_id_seq OWNER TO postgres;

--
-- TOC entry 2917 (class 0 OID 0)
-- Dependencies: 211
-- Name: synonymreference_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE synonymreference_id_seq OWNED BY synonymreference.id;


--
-- TOC entry 2744 (class 2604 OID 16733)
-- Name: action id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY action ALTER COLUMN id SET DEFAULT nextval('action_id_seq'::regclass);


--
-- TOC entry 2738 (class 2604 OID 16642)
-- Name: agent id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY agent ALTER COLUMN id SET DEFAULT nextval('agent_id_seq'::regclass);


--
-- TOC entry 2741 (class 2604 OID 16685)
-- Name: entity id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY entity ALTER COLUMN id SET DEFAULT nextval('entity_id_seq'::regclass);


--
-- TOC entry 2740 (class 2604 OID 16664)
-- Name: intent id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY intent ALTER COLUMN id SET DEFAULT nextval('intent_id_seq'::regclass);


--
-- TOC entry 2746 (class 2604 OID 16778)
-- Name: parameter id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY parameter ALTER COLUMN id SET DEFAULT nextval('parameter_id_seq'::regclass);


--
-- TOC entry 2739 (class 2604 OID 16653)
-- Name: priority id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY priority ALTER COLUMN id SET DEFAULT nextval('priority_id_seq'::regclass);


--
-- TOC entry 2742 (class 2604 OID 16701)
-- Name: question id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY question ALTER COLUMN id SET DEFAULT nextval('question_id_seq'::regclass);


--
-- TOC entry 2743 (class 2604 OID 16717)
-- Name: response id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY response ALTER COLUMN id SET DEFAULT nextval('response_id_seq'::regclass);


--
-- TOC entry 2747 (class 2604 OID 16799)
-- Name: synonym id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY synonym ALTER COLUMN id SET DEFAULT nextval('synonym_id_seq'::regclass);


--
-- TOC entry 2745 (class 2604 OID 16762)
-- Name: synonymreference id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY synonymreference ALTER COLUMN id SET DEFAULT nextval('synonymreference_id_seq'::regclass);


--
-- TOC entry 2761 (class 2606 OID 16738)
-- Name: action action_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY action
    ADD CONSTRAINT action_pkey PRIMARY KEY (id);


--
-- TOC entry 2749 (class 2606 OID 16647)
-- Name: agent agent_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY agent
    ADD CONSTRAINT agent_pkey PRIMARY KEY (id);


--
-- TOC entry 2755 (class 2606 OID 16690)
-- Name: entity entity_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY entity
    ADD CONSTRAINT entity_pkey PRIMARY KEY (id);


--
-- TOC entry 2753 (class 2606 OID 16669)
-- Name: intent intent_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY intent
    ADD CONSTRAINT intent_pkey PRIMARY KEY (id);


--
-- TOC entry 2765 (class 2606 OID 16783)
-- Name: parameter parameter_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY parameter
    ADD CONSTRAINT parameter_pkey PRIMARY KEY (id);


--
-- TOC entry 2751 (class 2606 OID 16658)
-- Name: priority priority_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY priority
    ADD CONSTRAINT priority_pkey PRIMARY KEY (id);


--
-- TOC entry 2757 (class 2606 OID 16706)
-- Name: question question_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY question
    ADD CONSTRAINT question_pkey PRIMARY KEY (id);


--
-- TOC entry 2759 (class 2606 OID 16722)
-- Name: response response_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY response
    ADD CONSTRAINT response_pkey PRIMARY KEY (id);


--
-- TOC entry 2767 (class 2606 OID 16804)
-- Name: synonym synonym_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY synonym
    ADD CONSTRAINT synonym_pkey PRIMARY KEY (id);


--
-- TOC entry 2763 (class 2606 OID 16767)
-- Name: synonymreference synonymreference_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY synonymreference
    ADD CONSTRAINT synonymreference_pkey PRIMARY KEY (id);


--
-- TOC entry 2773 (class 2606 OID 16739)
-- Name: action action_intent_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY action
    ADD CONSTRAINT action_intent_id_fkey FOREIGN KEY (intent_id) REFERENCES intent(id);


--
-- TOC entry 2770 (class 2606 OID 16691)
-- Name: entity entity_agent_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY entity
    ADD CONSTRAINT entity_agent_id_fkey FOREIGN KEY (agent_id) REFERENCES agent(id);


--
-- TOC entry 2775 (class 2606 OID 16752)
-- Name: entityrelationship entityrelationship_childid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY entityrelationship
    ADD CONSTRAINT entityrelationship_childid_fkey FOREIGN KEY (childid) REFERENCES entity(id);


--
-- TOC entry 2774 (class 2606 OID 16747)
-- Name: entityrelationship entityrelationship_parentid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY entityrelationship
    ADD CONSTRAINT entityrelationship_parentid_fkey FOREIGN KEY (parentid) REFERENCES entity(id);


--
-- TOC entry 2768 (class 2606 OID 16670)
-- Name: intent intent_agent_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY intent
    ADD CONSTRAINT intent_agent_id_fkey FOREIGN KEY (agent_id) REFERENCES agent(id);


--
-- TOC entry 2769 (class 2606 OID 16675)
-- Name: intent intent_priority_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY intent
    ADD CONSTRAINT intent_priority_id_fkey FOREIGN KEY (priority_id) REFERENCES priority(id);


--
-- TOC entry 2777 (class 2606 OID 16784)
-- Name: parameter parameter_action_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY parameter
    ADD CONSTRAINT parameter_action_id_fkey FOREIGN KEY (action_id) REFERENCES action(id);


--
-- TOC entry 2778 (class 2606 OID 16789)
-- Name: parameter parameter_entity_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY parameter
    ADD CONSTRAINT parameter_entity_id_fkey FOREIGN KEY (entity_id) REFERENCES entity(id);


--
-- TOC entry 2771 (class 2606 OID 16707)
-- Name: question question_intent_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY question
    ADD CONSTRAINT question_intent_id_fkey FOREIGN KEY (intent_id) REFERENCES intent(id);


--
-- TOC entry 2772 (class 2606 OID 16723)
-- Name: response response_intent_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY response
    ADD CONSTRAINT response_intent_id_fkey FOREIGN KEY (intent_id) REFERENCES intent(id);


--
-- TOC entry 2779 (class 2606 OID 16805)
-- Name: synonym synonym_synonymreference_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY synonym
    ADD CONSTRAINT synonym_synonymreference_id_fkey FOREIGN KEY (synonymreference_id) REFERENCES synonymreference(id);


--
-- TOC entry 2776 (class 2606 OID 16768)
-- Name: synonymreference synonymreference_entity_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY synonymreference
    ADD CONSTRAINT synonymreference_entity_id_fkey FOREIGN KEY (entity_id) REFERENCES entity(id);


-- Completed on 2017-11-23 23:00:00

--
-- PostgreSQL database dump complete
--

